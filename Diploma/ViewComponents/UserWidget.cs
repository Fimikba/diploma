﻿using DataAccess.Context;
using Diploma.Models;
using DomainServices.WorkContexts;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace Diploma.ViewComponents
{
    public class UserWidget : ViewComponent
    {
        private WorkContextAccessor _workContextAccessor;
        private DataContext _dataContext;

        public UserWidget(WorkContextAccessor workContextAccessor, DataContext dataContext)
        {
            _workContextAccessor = workContextAccessor;
            _dataContext = dataContext;
        }

        public IViewComponentResult Invoke()
        {
            var userId = _workContextAccessor.WorkContext.UserId;
            var userWidgetModel = new UserWidgetModel();

            if (userId.HasValue)
            {
                userWidgetModel.UserId = userId;
                userWidgetModel.FirstName = _dataContext.Users.FirstOrDefault(u => u.Id == userId).FirstName;
            }

            return View(userWidgetModel);
        }
    }
}
