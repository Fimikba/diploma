﻿using DataAccess.Context;
using Diploma.Models;
using DomainServices.WorkContexts;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diploma.ViewComponents
{
    public class TopicsMenu : ViewComponent
    {
        private WorkContextAccessor _workContextAccessor;
        private DataContext _dataContext;

        public TopicsMenu(WorkContextAccessor workContextAccessor, DataContext dataContext)
        {
            _workContextAccessor = workContextAccessor;
            _dataContext = dataContext;
        }

        public IViewComponentResult Invoke()
        {
            var topicsMenuModel = _dataContext.Topics.Select(t => new TopicsMenuModel {Id=t.Id, Name=t.Name }).ToList();
            return View(topicsMenuModel);
        }
    }
}
