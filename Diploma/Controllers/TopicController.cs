﻿using DataAccess.Context;
using Diploma.Models;
using Domain.Models.Companies;
using Domain.Models.Files;
using Domain.Models.Topics;
using DomainServices.WorkContexts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diploma.Controllers
{
    public class TopicController : BaseController
    {
        private DataContext _dataContext;
        private WorkContextAccessor _workContextAccessor;
        private IWebHostEnvironment _webHostEnvironment;

        public TopicController(WorkContextAccessor workContextAccessor, DataContext dataContext, IWebHostEnvironment webHostEnvironment)
            : base(workContextAccessor, dataContext)
        {
            _dataContext = dataContext;
            _workContextAccessor = workContextAccessor;
            _webHostEnvironment = webHostEnvironment;
        }

        public IActionResult Index(int? page)
        {
            var topics = new List<Topic>();

            topics.AddRange(
                _dataContext.StandartTopics
                .WithPreview()
                .ToList());

            var company =
                _dataContext.Companies.WithCreatedAndUsedCustomeTopics()
                .FirstOrDefault(c => c.Id == _workContextAccessor.WorkContext.CompanyId);
            if (company != null)
            {
                topics.AddRange(company.CreatedTopics);
                topics.AddRange(company.CustomTopics);
            }

            var indexTopicModel = new IndexTopicModel();
            foreach (var topic in topics)
            {
                indexTopicModel.CardTopicModels.Add(new CardTopicModel
                {
                    Id = topic.Id,
                    Name = topic.Name,
                    Description = topic.Description,
                    Preview = topic.Preview
                });
            }
            indexTopicModel.IsCompanyAdmin = _workContextAccessor.WorkContext.IsCompanyAdmin;

            return View(indexTopicModel);
        }

        [Authorize(Roles ="company_admin")]
        [HttpGet]
        public IActionResult Add()
        {
            return View();
        }

        [Authorize(Roles = "company_admin")]
        [HttpPost]
        public async Task<IActionResult> Add(TopicAddModel topic)
        {
            if (ModelState.IsValid)
            {
                var companyId = _workContextAccessor.WorkContext.CompanyId;

                File file = null;
                if (topic.Prewiew != null)
                {
                    var path = topic.Prewiew.FileName;
                    using (var fileStream = new System.IO.FileStream(System.IO.Path.Combine(_webHostEnvironment.WebRootPath, "Files", "Topics", "Previewes", path), System.IO.FileMode.Create))
                    {
                        await topic.Prewiew.CopyToAsync(fileStream);
                    }

                    file = new File
                    {
                        Name = path,
                        Path = "~/Files/Topics/Previewes" + topic.Prewiew.FileName
                    };
                }

                _dataContext.Add(new CustomTopic
                {
                    Name = topic.Name,
                    Description = topic.Description,
                    Preview = file ?? null,
                    AuthoredCompany = _dataContext.Companies.FirstOrDefault(c => c.Id == companyId)
                });
                await _dataContext.SaveChangesAsync();

                return RedirectToAction("Index", "Topic");
            }
            return View(topic);
        }
    }
}