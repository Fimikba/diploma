﻿using DataAccess.Context;
using Diploma.Models;
using Domain.Models.Tests;
using Domain.Models.UserTests;
using DomainServices.Games;
using DomainServices.WorkContexts;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diploma.Controllers
{
    public class TestController : BaseController
    {
        private DataContext _dataContext;
        private WorkContextAccessor _workContextAccessor;
        private IWebHostEnvironment _webHostEnvironment;
        private GameServices _gameServices;

        public TestController(DataContext dataContext, WorkContextAccessor workContextAccessor, IWebHostEnvironment webHostEnvironment, GameServices gameServices) : base(workContextAccessor, dataContext)
        {
            _dataContext = dataContext;
            _workContextAccessor = workContextAccessor;
            _webHostEnvironment = webHostEnvironment;
            _gameServices = gameServices;
        }


        [HttpGet]
        public async Task<IActionResult> GetNextQuestion(int testId)
        {
            if (_dataContext.UserTests.FirstOrDefault(ut => ut.TestId == testId && ut.UserId == _workContextAccessor.WorkContext.UserId) != null)
            {
                return View("TestPased");
            }

            UserTestTempDataModel userTestTempDataModel;
            if (TempData.ContainsKey($"test_{testId}"))
            {
                userTestTempDataModel = JsonConvert.DeserializeObject<UserTestTempDataModel>(TempData[$"test_{testId}"]?.ToString());
            }
            else
            {
                userTestTempDataModel = new UserTestTempDataModel(
                userTest: new UserTest
                {
                    UserId = _workContextAccessor.WorkContext.UserId.Value,
                    TestId = testId,
                    UserTestQuestions = new List<UserTestQuestion>()
                },
                startTime: DateTime.Now);
            }

            var maxTime = _dataContext.Tests.FirstOrDefault(t => t.Id == testId).MaxTime;
            if (DateTime.Now.Subtract(userTestTempDataModel.StartTime).CompareTo(maxTime) >= 0)
            {
                userTestTempDataModel.UserTest.Points = 0;
                _dataContext.UserTests.Add(userTestTempDataModel.UserTest);
                TempData.Remove($"test_{testId}");
                await _dataContext.SaveChangesAsync();

                return View("TimeIsOver");
            }

            var questions = _dataContext
                    .Tests
                    .WithQuestions()
                    .Select(t => new { Id = t.Id, Questions = t.Questions })
                    .FirstOrDefault(x => x.Id == testId)
                    .Questions;

            var nextQuestionId = userTestTempDataModel.UserTest.UserTestQuestions.Count > 0
                ? userTestTempDataModel.UserTest.UserTestQuestions.OrderBy(utq => utq.QuestionId).Last().QuestionId + 1
                : questions
                    .OrderBy(q => q.Id)
                    .First()
                    .Id;

            var nextQuestion =
                questions
                .FirstOrDefault(q => q.Id == nextQuestionId);

            TempData[$"test_{testId}"] = JsonConvert.SerializeObject(userTestTempDataModel);

            var answers = new List<AnswerModel>();
            foreach (var answer in nextQuestion.Answers)
            {
                answers.Add(new AnswerModel { Id = answer.Id, Text = answer.Text });
            }
            var questionModel = new QuestionModel()
            {
                Id = nextQuestion.Id,
                TestId = testId,
                Type = nextQuestion.Type,
                Text = nextQuestion.Text,
                Images = nextQuestion.Images.ToList(),
                Answers = answers,
                MaxPoints = nextQuestion.Points().Value
            };

            return View(questionModel);
        }

        [HttpPost]
        public async Task<IActionResult> GetNextQuestion(int testId, int questionId, int[] selectedAnswers)
        {
            var userTestTempDataModel = JsonConvert.DeserializeObject<UserTestTempDataModel>(TempData[$"test_{testId}"]?.ToString());
            userTestTempDataModel.UserTest.UserTestQuestions.Add(new UserTestQuestion
            {
                QuestionId = questionId
            });

            var question = _dataContext
            .Tests
            .WithQuestions()
            .Select(t => new { Id = t.Id, Questions = t.Questions })
            .FirstOrDefault(x => x.Id == testId)
            .Questions
            .FirstOrDefault(q => q.Id == questionId);

            var maxTime = _dataContext.Tests.FirstOrDefault(t => t.Id == testId).MaxTime;
            if (DateTime.Now.Subtract(userTestTempDataModel.StartTime).CompareTo(maxTime) >= 0)
            {
                userTestTempDataModel.UserTest.Points = 0;
                _dataContext.UserTests.Add(userTestTempDataModel.UserTest);
                TempData.Remove($"test_{testId}");
                await _dataContext.SaveChangesAsync();

                return View("TimeIsOver");
            }

            int points = 0;
            foreach (var answer in question.Answers)
            {
                if (selectedAnswers.Contains(answer.Id))
                {
                    points += answer.Points;
                }
            }
            var curentQuestion = userTestTempDataModel.UserTest.UserTestQuestions.FirstOrDefault(utq => utq.QuestionId == questionId);
            curentQuestion.Points = points;

            TempData[$"test_{testId}"] = JsonConvert.SerializeObject(userTestTempDataModel);

            var lastQuestionId = _dataContext
            .Tests
            .WithQuestions()
            .Select(t => new { Id = t.Id, Questions = t.Questions })
            .FirstOrDefault(x => x.Id == testId)
            .Questions
            .OrderBy(q => q.Id)
            .Last()
            .Id;

            if (curentQuestion.QuestionId == lastQuestionId)
            {
                userTestTempDataModel.UserTest.Points = userTestTempDataModel.UserTest.UserTestQuestions.Sum(utq => utq.Points);
                _dataContext.UserTests.Add(userTestTempDataModel.UserTest);
                TempData.Remove($"test_{testId}");
                await _dataContext.SaveChangesAsync();

                return View("End");
            }
            else
            {
                return RedirectToAction("GetNextQuestion", "Test", new { testId = testId });
            }
        }

        //public async Task<IActionResult> Add()
        //{
        //    return View("AddTest");
        //}
    }
}
