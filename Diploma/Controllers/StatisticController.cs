﻿using DataAccess.Context;
using Diploma.Models;
using Domain.Models.Users;
using Domain.Models.UserTests;
using DomainServices.Users;
using DomainServices.WorkContexts;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System;

namespace Diploma.Controllers
{
    public class StatisticController : BaseController
    {
        private UserManager<User> _userManager;
        private DataContext _dataContext;
        private SignInManager<User> _signInManager;
        private UserServices _userServices;

        public StatisticController(WorkContextAccessor workContextAccessor, UserManager<User> userManager, DataContext dataContext,
            UserServices userServices, SignInManager<User> signInManager = null)
            : base(workContextAccessor, dataContext)
        {
            _userManager = userManager;
            _dataContext = dataContext;
            _signInManager = signInManager;
            _userServices = userServices;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            if (_workContextAccessor.WorkContext.IsCompanyMember == true)
            {
                var statisticModels = new List<StatisticModel>();

                var userTests = _dataContext.UserTests
                    .WithTest()
                    .Where(ut => ut.UserId == _workContextAccessor.WorkContext.UserId)
                    .Select(ut => new { Points = ut.Points, Test = ut.Test, UserId = ut.UserId})
                    .ToList();

                foreach (var userTest in userTests)
                {
                    statisticModels.Add(new StatisticModel
                    {
                        TestId = userTest.Test.Id,
                        UserId = userTest.UserId,
                        TestName = userTest.Test.Name,
                        TestStage = userTest.Test.TopicStage,
                        UserPoints = userTest.Points.Value,
                        MaxPointsForTest = userTest.Test.MaxPoints(),
                        PointInPersent = Math.Round( ((float) userTest.Points.Value / (float) userTest.Test.MaxPoints()*100),2)
                    });
                }

                return View("IndexForMember",statisticModels);

            }
            else if (_workContextAccessor.WorkContext.IsCompanyAdmin == true)
            {
                var statisticAdminModels = new List<StatisticModelForAdmin>();
                var adminId = _workContextAccessor.WorkContext.CompanyId;

                var userTestsForAdmin = _dataContext.UserTests
                    .WithTest()
                    .Where(ut => ut.User.Company.Id == adminId)
                    .Select(ut => new { UserName = ut.User, Points = ut.Points, Test = ut.Test })
                    .ToList();

                foreach (var userTest in userTestsForAdmin)
                {
                    statisticAdminModels.Add(new StatisticModelForAdmin
                    {
                        UserName = userTest.UserName.Surname,
                        TestName = userTest.Test.Name,
                        TestStage = userTest.Test.TopicStage,
                        UserPoints = userTest.Points.Value,
                        MaxPointsForTest = userTest.Test.MaxPoints(),
                        PointInPersent = Math.Round(((float)userTest.Points.Value / (float)userTest.Test.MaxPoints() * 100), 2)
                    });
                }

                return View("IndexForAdmin", statisticAdminModels);
            }
            return View();
        }

        public async Task<IActionResult> FullStatistic(int testId, int userId)
        {
            var fullStatistic = new StatisticTestModel();

            var userTest = _dataContext.UserTests
                .WithTest()
                .WithQuestion()
                .FirstOrDefault(ut => ut.TestId == testId && ut.UserId == userId);

            fullStatistic.TestId = userTest.TestId;

            foreach (var question in userTest.Test.Questions)
            {
                fullStatistic.StatisticQuestionModels.Add(new StatisticQuestionModel
                {
                    QuestionId = question.Id,
                    QuestionText = question.Text,
                    Answers = question.Answers.Select(a => new AnswerModel(a.Id, a.Text)).ToList(),
                    CorrectAnswerIds = question.Answers.Where(a => a.Points > 0).Select(a => a.Id).ToList(),
                    UserPoints = userTest.UserTestQuestions.FirstOrDefault(utq => utq.QuestionId == question.Id).Points,
                    MaxPointsForQuestion = question.Points().Value
                });
            }

            return View(fullStatistic);
        }
    }
}
