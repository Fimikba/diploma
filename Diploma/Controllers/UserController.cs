﻿using DataAccess.Context;
using Domain.Models.Roles;
using Domain.Models.Users;
using DomainServices.Users;
using DomainServices.WorkContexts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using SignInResult = Microsoft.AspNetCore.Identity.SignInResult;

namespace Diploma.Controllers
{
    public class UserController : BaseController
    {
        private UserManager<User> _userManager;
        private DataContext _dataContext;
        private SignInManager<User> _signInManager;
        private UserServices _userServices;

        public UserController(WorkContextAccessor workContextAccessor, UserManager<User> userManager, DataContext dataContext,
            UserServices userServices, SignInManager<User> signInManager = null)
            :base(workContextAccessor, dataContext)
        {
            _userManager = userManager;
            _dataContext = dataContext;
            _signInManager = signInManager;
            _userServices = userServices;
        }

        [AllowAnonymous]
        public IActionResult Registr()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Registr(UserRegistrModel model)
        {
            if (ModelState.IsValid)
            {
                IdentityResult resultRegistr = await _userManager.CreateAsync(new User
                {
                    FirstName = model.FirstName,
                    SecondName = model.SecondName,
                    Surname = model.Surname,
                    Email = model.Email,
                    UserName = model.Email
                }, model.Password);

                if (resultRegistr.Succeeded)
                {
                    var user = _dataContext.Users.FirstOrDefault(u => u.Email == model.Email);
                    if(await _userServices.AddUserToCompany(user, model.RegistrationToken))
                    {
                        await _dataContext.SaveChangesAsync();
                        return RedirectToAction("Login", "User");
                    }

                    ModelState.AddModelError("", "Невозможно присоединить пользователя к компании!");
                }
                else
                {
                    foreach (var error in resultRegistr.Errors)
                    {
                        ModelState.AddModelError("", error.Description);
                    }
                }

            }
            return View(model);
        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult Login(string returnUrl)
        {
            return View(new UserLoginModel {
                ReturnUrl = returnUrl ?? ""
            });
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Login(UserLoginModel model)
        {
            if (ModelState.IsValid)
            {
                User user = await _userManager.FindByEmailAsync(model.Email);
                if (user != null)
                {
                    if (User!=null)
                    {
                        await _signInManager.SignOutAsync();
                    }
                    SignInResult result = await _signInManager.PasswordSignInAsync(user, model.Password, false, false);
                    if (result.Succeeded)
                    {
                        string returnUrl = !string.IsNullOrEmpty(model.ReturnUrl) ? "~" + model.ReturnUrl : "~/topic";
                        return Redirect(returnUrl);
                    }
                }
                ModelState.AddModelError("", "Invalid user email or password");
            }
            return View(model);
        }

        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }
    }
} 
