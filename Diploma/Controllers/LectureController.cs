﻿using DataAccess.Context;
using Diploma.Models;
using Domain.Models.Files;
using Domain.Models.Games;
using Domain.Models.Lectures;
using DomainServices.Games;
using DomainServices.WorkContexts;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Diploma.Controllers
{
    public class LectureController : BaseController
    {
        private DataContext _dataContext;
        private WorkContextAccessor _workContextAccessor;
        private IWebHostEnvironment _webHostEnvironment;

        public LectureController(WorkContextAccessor workContextAccessor, DataContext dataContext, IWebHostEnvironment webHostEnvironment)
            : base(workContextAccessor, dataContext)
        {
            _dataContext = dataContext;
            _workContextAccessor = workContextAccessor;
            _webHostEnvironment = webHostEnvironment;
        }

        [HttpGet]
        public IActionResult Add(int gameId)
        {
            var addLectureModel = new AddLectureModel()
            {
                GameId = gameId
            };

            return View(addLectureModel);
        }

        [HttpPost]
        public async Task<IActionResult> Add(AddLectureModel addLectureModel)
        {
            if (ModelState.IsValid)
            {
                var companyId = _workContextAccessor.WorkContext.CompanyId;

                Image previewFile = null;
                if (addLectureModel.Preview != null)
                {
                    var fileName = addLectureModel.Preview.FileName;
                    using (var fileStream = new System.IO.FileStream(System.IO.Path.Combine(_webHostEnvironment.WebRootPath, "Files", "Lectures", "Previewes", fileName), System.IO.FileMode.Create))
                    {
                        await addLectureModel.Preview.CopyToAsync(fileStream);
                    }

                    previewFile = new Image
                    {
                        Name = fileName,
                        Path = "~/Files/Lectures/Previewes" + addLectureModel.Preview.FileName
                    };
                }

                Domain.Models.Files.File materialFile = null;
                if (addLectureModel.Material != null)
                {
                    var fileName = addLectureModel.Material.FileName;
                    using (var fileStream = new System.IO.FileStream(System.IO.Path.Combine(_webHostEnvironment.WebRootPath, "Files", "Lectures", "Materials", fileName), System.IO.FileMode.Create))
                    {
                        await addLectureModel.Material.CopyToAsync(fileStream);
                    }

                    materialFile = new Domain.Models.Files.File
                    {
                        Name = fileName,
                        Path = "~/Files/Lectures/Materials" + addLectureModel.Material.FileName
                    };
                }

                var games = new List<Game>();
                games.Add(_dataContext.Games.FirstOrDefault(g => g.Id == addLectureModel.GameId));

                _dataContext.Add(new CustomLecture
                {
                    LectureName = addLectureModel.Name,
                    Preview = previewFile,
                    Material = materialFile,
                    Games = games,
                    AuthorCompany = _dataContext.Companies.FirstOrDefault(c => c.Id == companyId),
                    IsPublic = addLectureModel.IsPublic
                });
                await _dataContext.SaveChangesAsync();

                return RedirectToAction("Details", "Game", new {gameId = addLectureModel.GameId, partType = GamePartType.Lecture});
            }

            return View(addLectureModel);
        }

        public IActionResult GetLecture(int lectureId)
        {
            var lectureMaterial = _dataContext.Lectures.WithMaterial().FirstOrDefault(l => l.Id == lectureId).Material;
            var filePath = Path.Combine("Files", "Lectures", "Materials", lectureMaterial.Name);

            return File(filePath, "text/plain", lectureMaterial.Name);
        }
    }
}
