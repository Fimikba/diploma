﻿using DataAccess.Context;
using Diploma.Models;
using Domain.Models.Files;
using Domain.Models.Games;
using DomainServices.WorkContexts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using DomainServices.Games;

namespace Diploma.Controllers
{
    public class GameController : BaseController
    {
        private DataContext _dataContext;
        private WorkContextAccessor _workContextAccessor;
        private IWebHostEnvironment _webHostEnvironment;
        private GameServices _gameServices;

        public GameController(DataContext dataContext, WorkContextAccessor workContextAccessor, IWebHostEnvironment webHostEnvironment, GameServices gameServices) : base(workContextAccessor, dataContext)
        {
            _dataContext = dataContext;
            _workContextAccessor = workContextAccessor;
            _webHostEnvironment = webHostEnvironment;
            _gameServices = gameServices;
        }

        public IActionResult Index(int? page, int topicId)
        {
            var games = new List<Game>();
            var companyId = _workContextAccessor.WorkContext.CompanyId;

            games.AddRange(_dataContext.Games.WithPreviewes().Where(g => g.Topic.Id == topicId && g.Company.Id == companyId).ToList());

            var indexGameModel = new IndexGameModel(); 
            foreach (var game in games)
            {
                indexGameModel.CardTopicModels.Add(new CardGameModel
                {
                    Id = game.Id,
                    Name = game.Name,
                    Preview = game.Preview
                });
            }
            indexGameModel.IsCompanyAdmin = _workContextAccessor.WorkContext.IsCompanyAdmin;
            indexGameModel.TopicId = topicId;

            return View(indexGameModel);
        }

        [Authorize(Roles = "company_admin")]
        [HttpGet]
        public IActionResult Add(int topicId)
        {
            var model = new GameAddModel()
            {
                TopicId = topicId
            };
            return View(model);
        }

        [Authorize(Roles = "company_admin")]
        [HttpPost]
        public async Task<IActionResult> Add(GameAddModel game)
        {
            if (ModelState.IsValid)
            {
                var companyId = _workContextAccessor.WorkContext.CompanyId;

                Image image = null;
                if (game.Prewiew != null)
                {
                    var path = game.Prewiew.FileName;
                    using (var fileStream = new System.IO.FileStream(System.IO.Path.Combine(_webHostEnvironment.WebRootPath, "Files", "Games", "Previewes", path), System.IO.FileMode.Create))
                    {
                        await game.Prewiew.CopyToAsync(fileStream);
                    }

                    image = new Image
                    {
                        Name = path,
                        Path = "~/Files/Games/Previewes" + game.Prewiew.FileName
                    };
                }
                
                _dataContext.Add(new Game
                {
                    Name = game.Name,
                    Preview = image ?? null,
                    Company = _dataContext.Companies.FirstOrDefault(c => c.Id == companyId),
                    Topic = _dataContext.Topics.FirstOrDefault(t => t.Id == game.TopicId)
                });
                await _dataContext.SaveChangesAsync();

                return RedirectToAction("Index", "Game", new {TopicId = game.TopicId});
            }
            return View(game);
        }

        public IActionResult Details(int gameId, GamePartType partType)
        {
            var gameDetailsModel = new GameDetailsModel()
            {
                GameId = gameId,
                GamePartType = partType,
                IsCompanyAdmin = _workContextAccessor.WorkContext.IsCompanyAdmin
            };

            switch (partType)
            {
                case GamePartType.PreliminaryTest:
                    gameDetailsModel.PreliminaryTests = _gameServices.GetPreliminaryTest(gameId);
                    break;
                case GamePartType.Lecture:
                    gameDetailsModel.Lectures = _gameServices.GetLecture(gameId);
                    break;
                case GamePartType.Game:
                    gameDetailsModel.GeneralTests = _gameServices.GetGeneralTest(gameId);
                    break;
                case GamePartType.FinalTest:
                    gameDetailsModel.FinalyTests= _gameServices.GetFinalyTest(gameId);
                    break;
            }

            return View(gameDetailsModel);
        }
    }
}
