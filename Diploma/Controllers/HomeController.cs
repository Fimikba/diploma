﻿using DataAccess.Context;
using Diploma.Models;
using DomainServices.WorkContexts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Diagnostics;

namespace Diploma.Controllers
{
    public class HomeController : BaseController
    {
        private readonly ILogger<HomeController> _logger;
        private WorkContextAccessor _workContextAccessor;

        public HomeController(WorkContextAccessor workContextAccessor, ILogger<HomeController> logger, DataContext dataContext)
            :base(workContextAccessor, dataContext)
        {
            _logger = logger;
            _workContextAccessor = workContextAccessor;
        }

        [AllowAnonymous]
        public IActionResult Index()
        {
            if (_workContextAccessor.WorkContext.UserId.HasValue)
            {
                return RedirectToAction("Index", "Topic");
            }

            return View();
        }

        [AllowAnonymous]
        public IActionResult Privacy()
        {
            return View();
        }

        [AllowAnonymous]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
