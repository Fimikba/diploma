﻿using DataAccess.Context;
using DomainServices.WorkContexts;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Linq;
using System.Security.Claims;
using Domain.Models.Users;
using Microsoft.AspNetCore.Authorization;

namespace Diploma.Controllers
{
    [Authorize]
    public class BaseController : Controller
    {
        public WorkContextAccessor _workContextAccessor;
        private DataContext _dataContext;

        public BaseController(WorkContextAccessor workContextAccessor, DataContext dataContext)
        {
            _workContextAccessor = workContextAccessor;
            _dataContext = dataContext;
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            base.OnActionExecuting(context);

            _workContextAccessor.WorkContext = SetWorkContext();
        }

        private WorkContext SetWorkContext()
        {
            var userId = GetUserId();
            var roles = GetRoles();

            return new WorkContext(
                userId: userId,
                companyId: userId.HasValue ? GetCompany(userId.Value) : null,
                isAdmin: roles.Contains("admin"),
                isCompanyAdmin: roles.Contains("company_admin"),
                isCompanyMember: roles.Contains("company_member"));
        }

        private int? GetUserId()
        {
            if (int.TryParse(User?.FindFirst(c => c.Type == ClaimTypes.NameIdentifier)?.Value, out int userId))
            {
                return userId;
            }

            return null;
        }

        private int? GetCompany(int userId)
        {
            return _dataContext.Users.UserWithCompany().FirstOrDefault(u => u.Id == userId)?.Company?.Id;
        }

        private string[] GetRoles()
        {
            return User?.FindAll(c => c.Type == ClaimTypes.Role)
                .Select(c => c.Value)
                .ToArray() ?? Array.Empty<string>();
        }


    }
}
