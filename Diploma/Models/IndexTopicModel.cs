﻿using System.Collections.Generic;

namespace Diploma.Models
{
    public class IndexTopicModel
    {
        public List<CardTopicModel> CardTopicModels { get; set; } = new List<CardTopicModel>();
        public bool IsCompanyAdmin { get; set; }
    }
}
