﻿namespace Diploma.Models
{
    public class AnswerModel
    {
        public AnswerModel() { }

        public AnswerModel(int id, string text)
        {
            Id = id;
            Text = text;
        }

        public int Id { get; set; }
        public string Text { get; set; }
    }
}
