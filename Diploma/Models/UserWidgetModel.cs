﻿namespace Diploma.Models
{
    public class UserWidgetModel
    {
        public int? UserId { get; set; }
        public string FirstName { get; set; }
    }
}
