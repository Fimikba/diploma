﻿using System.Collections.Generic;

namespace Diploma.Models
{
    public class StatisticQuestionModel
    {
        public int QuestionId { get; set; }
        public string QuestionText { get; set; }
        public ICollection<AnswerModel> Answers { get; set; }
        public ICollection<int> CorrectAnswerIds { get; set; }
        public int UserPoints { get; set; }
        public int MaxPointsForQuestion { get; set; }
    }
}
 