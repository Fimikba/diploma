﻿using Domain.Models.UserTests;
using System;

namespace Diploma.Models
{
    public class UserTestTempDataModel
    {
        public UserTestTempDataModel(UserTest userTest, DateTime startTime)
        {
            UserTest = userTest;
            StartTime = startTime;
        }

        public UserTest UserTest { get; set; }
        public DateTime StartTime { get; set; }
    }
}
