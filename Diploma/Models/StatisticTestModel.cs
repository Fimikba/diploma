﻿using System.Collections.Generic;

namespace Diploma.Models
{
    public class StatisticTestModel
    {
        public int TestId { get; set; }
        public List<StatisticQuestionModel> StatisticQuestionModels { get; set; } = new List<StatisticQuestionModel>();
    }
}
