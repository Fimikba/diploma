﻿using Domain.Models.Tests;

namespace Diploma.Models
{
    public class StatisticModelForAdmin
    {
        public string UserName { get; set; }
        public string TestName { get; set; }
        public TopicStage TestStage { get; set; }
        public int UserPoints { get; set; }
        public int MaxPointsForTest { get; set; }
        public double PointInPersent { get; set; }
    }
}
