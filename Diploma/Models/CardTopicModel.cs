﻿using Domain.Models.Files;

namespace Diploma.Models
{
    public class CardTopicModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public File Preview { get; set; }
    }
}