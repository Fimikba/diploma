﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace Diploma.Models
{
    public class AddLectureModel
    {
        [Required]
        [Display(Name = "Название")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Доступна всем пользователям?")]
        public bool IsPublic { get; set; }

        [Required]
        [Display(Name = "Материал для лекции")]
        public IFormFile Material { get; set; }

        [Required]
        public int GameId { get; set; }

        [Display(Name = "Обложка")]
        public IFormFile Preview { get; set; }
    }
}