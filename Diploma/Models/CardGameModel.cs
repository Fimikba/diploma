﻿using Domain.Models.Files;

namespace Diploma.Models
{
    public class CardGameModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Image Preview { get; set; }
    }
}