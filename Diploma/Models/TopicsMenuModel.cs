﻿namespace Diploma.Models
{
    public class TopicsMenuModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
