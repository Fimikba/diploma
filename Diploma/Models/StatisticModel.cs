﻿using Domain.Models.Tests;

namespace Diploma.Models
{
    public class StatisticModel
    {
        public int TestId { get; set; }
        public int UserId { get; set; }
        public string TestName { get; set; }
        public TopicStage TestStage { get; set; }
        public int UserPoints { get; set; }
        public int MaxPointsForTest { get; set; }
        public double PointInPersent { get; set; }
    }
}