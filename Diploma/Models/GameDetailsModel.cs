﻿using Domain.Models.Lectures;
using Domain.Models.Tests;
using DomainServices.Games;
using System.Collections.Generic;

namespace Diploma.Models
{
    public class GameDetailsModel
    {
        public int GameId { get; set; }
        public GamePartType GamePartType { get; set; }
        public List<Lecture> Lectures { get; set; }
        public List<Test> PreliminaryTests { get; set; }
        public List<Test> GeneralTests { get; set; }
        public List<Test> FinalyTests { get; set; }
        public bool IsCompanyAdmin { get; set; }
    }
}
