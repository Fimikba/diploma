﻿using System.Collections.Generic;

namespace Diploma.Models
{
    public class IndexGameModel
    {
        public List<CardGameModel> CardTopicModels { get; set; } = new List<CardGameModel>();
        public bool IsCompanyAdmin { get; set; }
        public int TopicId { get; set; }
    }
}