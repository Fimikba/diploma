﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace Diploma.Models
{
    public class AddTestQuestionModel
    {
        [Required]
        [Display(Name = "Текст вопроса")]
        public string Text { get; set; }

        [Required]
        [Display(Name = "Ответ на вопрос")]
        public string AnswerText1 { get; set; }

        [Display(Name = "Баллы за ответ")]
        public int Points1 { get; set; }

        [Display(Name = "Ответ на вопрос")]
        public string AnswerText2 { get; set; }

        [Display(Name = "Баллы за ответ")]
        public int Points2 { get; set; }

        [Display(Name = "Ответ на вопрос")]
        public string AnswerText3 { get; set; }

        [Display(Name = "Баллы за ответ")]
        public int Points3 { get; set; }

        [Display(Name = "Ответ на вопрос")]
        public string AnswerText4 { get; set; }

        [Display(Name = "Баллы за ответ")]
        public int Points4 { get; set; }

        [Display(Name = "Ответ на вопрос")]
        public string AnswerText5 { get; set; }

        [Display(Name = "Баллы за ответ")]
        public int Points5 { get; set; }

        [Display(Name = "Ответ на вопрос")]
        public string AnswerText6 { get; set; }

        [Display(Name = "Баллы за ответ")]
        public int Points6 { get; set; }

        [Display(Name = "Ответ на вопрос")]
        public string AnswerText7 { get; set; }

        [Display(Name = "Баллы за ответ")]
        public int Points7 { get; set; }

        [Display(Name = "Ответ на вопрос")]
        public string AnswerText8 { get; set; }

        [Display(Name = "Баллы за ответ")]
        public int Points8 { get; set; }

        [Display(Name = "Ответ на вопрос")]
        public string AnswerText9 { get; set; }

        [Display(Name = "Баллы за ответ")]
        public int Points9 { get; set; }
   
        [Display(Name = "Ответ на вопрос")]
        public string AnswerText10 { get; set; }

        [Display(Name = "Баллы за ответ")]
        public int Points10 { get; set; }

        [Display(Name = "Ответ на вопрос")]
        public string AnswerText11 { get; set; }
       

        [Required]
        [Display(Name = "Доступнен всем пользователям?")]
        public bool IsPublic { get; set; }

        [Display(Name = "Картинки и схемы для вопроса")]
        public IFormFile Material1 { get; set; }
        [Display(Name = "Картинки и схемы для вопроса")]
        public IFormFile Material2 { get; set; }
        [Display(Name = "Картинки и схемы для вопроса")]
        public IFormFile Material3 { get; set; }
        [Display(Name = "Картинки и схемы для вопроса")]
        public IFormFile Material4 { get; set; }
        [Display(Name = "Картинки и схемы для вопроса")]
        public IFormFile Material5 { get; set; }

        [Required]
        [Display(Name = "Вопрос с множественным вариантом ответа?")]
        public bool IsMultyplyAnswer { get; set; }
        [Required]
        [Display(Name = "Вопрос с одним правильным ответом?")]
        public bool IsSingleAnswer { get; set; }

        [Required]
        public int GameId { get; set; }
        [Required]
        public int AuthoredCompany { get; set; }
        [Required]
        public int TestId { get; set; }
    }
}
