﻿using Domain.Models.Answers;
using Domain.Models.Files;
using Domain.Models.Questions;
using System.Collections.Generic;

namespace Diploma.Models
{
    public class QuestionModel
    {
        public int Id { get; set; }
        public int TestId { get; set; }
        public QuestionType Type { get; set; }
        public string Text { get; set; }
        public List<Image> Images { get; set; }
        public List<AnswerModel> Answers { get; set; }
        public int MaxPoints { get; set; }
    }
}
