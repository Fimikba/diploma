 using DataAccess.Context;
using Domain.Models.Users;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using DomainServices.WorkContexts;
using DomainServices.Users;
using DomainServices.Companies;
using DomainServices.Games;
using System;

namespace Diploma
{
    public class Startup
    {
        private readonly IHostEnvironment _env;
        public Startup(IHostEnvironment env)
        {
            _env = env;
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();

            Configuration = builder.Build();
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<DataContext>(options => 
            options.UseNpgsql(Configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<User, IdentityRole<int>>()
                .AddEntityFrameworkStores<DataContext>();

            services.ConfigureApplicationCookie(option =>
            {
                option.LoginPath = "/User/Login";
            });

            services.AddScoped<WorkContextAccessor>();

            services.AddScoped<UserServices>();
            services.AddScoped<CompanyServices>();
            services.AddScoped<GameServices>();

            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromHours(5);
            });

            services.AddControllersWithViews()
                .AddSessionStateTempDataProvider();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }
            app.UseHttpsRedirection();

            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseSession();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");

                endpoints.MapControllerRoute(
                    name: "indexRoute",
                    pattern: "{controller}/{action=Index}/{page?}");
            });
        }
    }
}