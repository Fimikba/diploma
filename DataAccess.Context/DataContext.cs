﻿using Microsoft.EntityFrameworkCore;
using Domain.Models.Answers;
using Domain.Models.Companies;
using Domain.Models.Files;
using Domain.Models.Tests;
using Domain.Models.Lectures;
using Domain.Models.Questions;
using Domain.Models.Topics;
using Domain.Models.UserTests;
using Domain.Models.UserLectures;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Domain.Models.Users;
using Microsoft.AspNetCore.Identity;
using Domain.Models.Teams;
using Domain.Models.UserTeams;
using Domain.Models.Games;
using Domain.Models.Games.GameTests.PreliminaryTestGames;
using Domain.Models.Games.GameTests.GeneralTestGames;
using Domain.Models.Games.GameTests.FinalyTestGames;

namespace DataAccess.Context
{
    public class DataContext : IdentityDbContext<User, IdentityRole<int>, int>
    {
        public DataContext(DbContextOptions<DataContext> options) 
            : base(options) { }

        public DbSet<Answer> Answers { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<File> Files { get; set; }
        public DbSet<Image> Images { get; set; }
        public DbSet<Test> Tests { get; set; }
        public DbSet<CustomeTest> CustomeTests { get; set; }
        public DbSet<StandartTest> StandartTests { get; set; }
        public DbSet<Lecture> Lectures { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<CustomQuestion> CustomeQuestions { get; set; }
        public DbSet<StandartQuestion> StandartQuestions { get; set; }
        public DbSet<Topic> Topics { get; set; }
        public DbSet<StandartTopic> StandartTopics { get; set; }
        public DbSet<CustomTopic> CustomTopics { get; set; }
        public DbSet<Game> Games { get; set; }
        public DbSet<UserTest> UserTests { get; set; }
        public DbSet<UserTest> UserTestQuestions { get; set; }
        public DbSet<UserLecture> UserLectures { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<UserTeam> UserTeams { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.ApplyConfiguration(new UserTestEntityTypeConfiguration());
            builder.ApplyConfiguration(new UserTestQuestionsEntityTypeConfiguration());

            builder.ApplyConfiguration(new UserLectureEntityTypeConfiguration());

            builder.ApplyConfiguration(new UserTeamEntityTypeConfiguration());

            builder.ApplyConfiguration(new CustomTopicEntityTypeConfiguration());

            builder.ApplyConfiguration(new PreliminaryTestGameEntityTypeConfiguration());
            builder.ApplyConfiguration(new GeneralTestGameEntityTypeConfiguration());
            builder.ApplyConfiguration(new FinalyTestGameEntityTypeConfiguration());
        }
    }
}