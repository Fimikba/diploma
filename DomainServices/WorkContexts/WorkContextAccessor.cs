﻿namespace DomainServices.WorkContexts
{
    public class WorkContextAccessor
    {
        private WorkContext _workContext;

        public WorkContext WorkContext 
        { 
            get => _workContext;
            set
            {
                if (_workContext == null)
                {
                    _workContext = value;
                }
            }
        }
    }
}