﻿using Domain.Models.Companies;

namespace DomainServices.WorkContexts
{
    public class WorkContext
    {
        public WorkContext(int? userId, int? companyId, bool isAdmin, bool isCompanyAdmin, bool isCompanyMember)
        {
            UserId = userId;
            IsAdmin = isAdmin;
            IsCompanyAdmin = isCompanyAdmin;
            IsCompanyMember = isCompanyMember;
            CompanyId = companyId;
        }

        public int? UserId { get; }
        public int? CompanyId { get; }
        public bool IsAdmin { get; }
        public bool IsCompanyAdmin { get; }
        public bool IsCompanyMember { get; }
    }
} 