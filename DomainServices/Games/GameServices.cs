﻿using DataAccess.Context;
using Domain.Models.Games;
using Domain.Models.Lectures;
using Domain.Models.Tests;
using DomainServices.WorkContexts;
using System.Collections.Generic;
using System.Linq;

namespace DomainServices.Games
{
    public class GameServices
    {
        private DataContext _dataContext;
        private WorkContextAccessor _workContextAccessor;

        public GameServices(DataContext dataContext, WorkContextAccessor workContextAccessor)
        {
            _dataContext = dataContext;
            _workContextAccessor = workContextAccessor;
        }

        public List<Test> GetPreliminaryTest(int gameId)
        {
            var preliminaryTests = new List<Test>();

            preliminaryTests.AddRange(
                _dataContext.Games
                .WithPreliminaryTestGames()
                .SelectMany(g => g.PreliminaryTestGames.Select(ptg => ptg.Test))
                .ToList());

            return preliminaryTests;
        }

        public List<Test> GetGeneralTest(int gameId)
        {
            var generalTests = new List<Test>();

            generalTests.AddRange(
                _dataContext.Games
                .WithGeneralTestGames()
                .SelectMany(g => g.GeneralTestGames.Select(gtg => gtg.Test))
                .ToList());

            return generalTests;
        }

        public List<Test> GetFinalyTest(int gameId)
        {
            var finalyTests = new List<Test>();

            finalyTests.AddRange(
                _dataContext.Games
                .WithFinalyTestGames()
                .SelectMany(g => g.FinalyTestGames.Select(ftg => ftg.Test))
                .ToList());

            return finalyTests;
        }

        public List<Lecture> GetLecture(int gameId)
        {
            var lecture = new List<Lecture>();

            lecture.AddRange(_dataContext.Games
                .WithLectures()
                .SelectMany(g => g.Lectures)
                .ToList());

            return lecture;
        }
    }
}
