﻿namespace DomainServices.Games
{
    public enum GamePartType
    {
        PreliminaryTest = 1,
        Lecture = 2,
        Game = 3,
        FinalTest = 4
    }
}