﻿using DataAccess.Context;
using Domain.Models.Companies;
using Domain.Models.Roles;
using Domain.Models.Users;
using Microsoft.AspNetCore.Identity;
using System.Linq;
using System.Threading.Tasks;

namespace DomainServices.Users
{
    public class UserServices
    {
        private DataContext _dataContext;
        private UserManager<User> _userManager;

        public UserServices(DataContext dataContext, UserManager<User> userManager)
        {
            _dataContext = dataContext;
            _userManager = userManager;
        }

        public async Task<bool> AddUserToCompany(User user, string registrationToken)
        {
            string userRole;
            Company company;

            if ((company = _dataContext.Companies.FirstOrDefault(c => c.AdminPublicRegistrationToken == registrationToken)) != null)
            {
                userRole = RoleNames.CompanyAdmin;
            }
            else if ((company = _dataContext.Companies.FirstOrDefault(c => c.MemberPublicRegistrationToken == registrationToken)) != null)
            {
                userRole = RoleNames.CompanyMember;
            }
            else
            {
                return false;
            }

            user.Company = company;
            await _userManager.AddToRoleAsync(user, userRole);
            return true;
        }
    }
}
