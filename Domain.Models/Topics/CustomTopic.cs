﻿using Domain.Models.Companies;
using System.Collections.Generic;

namespace Domain.Models.Topics
{
    public class CustomTopic : Topic
    {
        public Company AuthoredCompany { get; set; }
        public ICollection<Company> ClientCompany { get; set; }
        public bool IsPublic { get; set; }
    }
}