﻿using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Domain.Models.Topics
{
    public static class TopicExtension
    {
        public static IQueryable<Topic> WithPreview(this IQueryable<Topic> query)
        {
            return query
                .Include(t => t.Preview);
        }
    }
}