﻿using Microsoft.EntityFrameworkCore;

namespace Domain.Models.Topics
{
    public class CustomTopicEntityTypeConfiguration : IEntityTypeConfiguration<CustomTopic>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<CustomTopic> builder)
        {
            builder.HasMany(ct => ct.ClientCompany)
                .WithMany(c => c.CustomTopics);

            builder.HasOne(ct => ct.AuthoredCompany)
                .WithMany(c => c.CreatedTopics);
        }
    }
}
