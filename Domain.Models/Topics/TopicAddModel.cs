﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace Domain.Models.Topics
{
    public class TopicAddModel
    {
        [Required]
        [Display(Name="Название")]
        public string Name { get; set; }

        [Display(Name = "Описание")]
        public string Description { get; set; }

        [Display(Name = "Обложка")]
        public IFormFile Prewiew { get; set; }

        [Required]
        [Display(Name = "Разрешить доступ всем пользователям?")]
        public bool IsPublic { get; set; }

    }
}