﻿using System.Collections.Generic;
using Domain.Models.Files;
using Domain.Models.Games;

namespace Domain.Models.Topics
{
    public class Topic
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public File Preview { get; set; }
        public ICollection<Game> Games { get; set; }
    }
}
