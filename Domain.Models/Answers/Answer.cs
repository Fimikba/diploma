﻿using Domain.Models.Questions;

namespace Domain.Models.Answers
{
    public class Answer
    {
        public int Id { get; set; }
        public int Points { get; set; }
        public string Text { get; set; }
        public Question Question { get; set; }
    }
}
