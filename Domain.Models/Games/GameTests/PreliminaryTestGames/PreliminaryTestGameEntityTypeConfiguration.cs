﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Domain.Models.Games.GameTests.PreliminaryTestGames
{
    public class PreliminaryTestGameEntityTypeConfiguration : IEntityTypeConfiguration<PreliminaryTestGame>
    {
        public void Configure(EntityTypeBuilder<PreliminaryTestGame> builder)
        {
            builder.HasKey(ptg => new { ptg.TestId, ptg.GameId });

            builder.HasOne(ptg => ptg.Test)
                .WithMany(t => t.PreliminaryTestGames)
                .HasForeignKey(ptg => ptg.TestId);

            builder.HasOne(ptg => ptg.Game)
                .WithMany(g => g.PreliminaryTestGames)
                .HasForeignKey(ptg => ptg.GameId);
        }
    }
}