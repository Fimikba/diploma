﻿using Domain.Models.Tests;

namespace Domain.Models.Games.GameTests.PreliminaryTestGames
{
    public class PreliminaryTestGame
    {
        public int TestId { get; set; }
        public Test Test { get; set; }
        public int GameId { get; set; }
        public Game Game { get; set; }
    }
}