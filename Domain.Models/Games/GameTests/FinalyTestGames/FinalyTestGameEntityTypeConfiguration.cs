﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Domain.Models.Games.GameTests.FinalyTestGames
{
    public class FinalyTestGameEntityTypeConfiguration : IEntityTypeConfiguration<FinalyTestGame>
    {
        public void Configure(EntityTypeBuilder<FinalyTestGame> builder)
        {
            builder.HasKey(ftg => new { ftg.TestId, ftg.GameId });

            builder.HasOne(ftg => ftg.Test)
                .WithMany(t => t.FinalyTestGames)
                .HasForeignKey(ftg => ftg.TestId);

            builder.HasOne(ftg => ftg.Game)
                .WithMany(g => g.FinalyTestGames)
                .HasForeignKey(ftg => ftg.GameId);
        }
    }
}