﻿using Domain.Models.Tests;

namespace Domain.Models.Games.GameTests.GeneralTestGames
{
    public class GeneralTestGame
    {
        public int TestId { get; set; }
        public Test Test { get; set; }
        public int GameId { get; set; }
        public Game Game { get; set; }
    }
}