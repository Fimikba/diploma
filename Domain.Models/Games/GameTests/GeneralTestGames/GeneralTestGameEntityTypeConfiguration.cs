﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Domain.Models.Games.GameTests.GeneralTestGames
{
    public class GeneralTestGameEntityTypeConfiguration : IEntityTypeConfiguration<GeneralTestGame>
    {
        public void Configure(EntityTypeBuilder<GeneralTestGame> builder)
        {
            builder.HasKey(gtg => new { gtg.TestId, gtg.GameId });

            builder.HasOne(gtg => gtg.Test)
                .WithMany(t => t.GeneralTestGames)
                .HasForeignKey(gtg => gtg.TestId);

            builder.HasOne(gtg => gtg.Game)
                .WithMany(g => g.GeneralTestGames)
                .HasForeignKey(gtg => gtg.GameId);
        }
    }
}