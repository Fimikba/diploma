﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace Domain.Models.Games
{
    public class GameAddModel
    {
        [Required]
        [Display(Name = "Название")]
        public string Name { get; set; }

        [Display(Name = "Обложка")]
        public IFormFile Prewiew { get; set; }

        [Required]
        public int TopicId { get; set; }
    }
}
