﻿using Domain.Models.Companies;
using Domain.Models.Files;
using Domain.Models.Games.GameTests.FinalyTestGames;
using Domain.Models.Games.GameTests.GeneralTestGames;
using Domain.Models.Games.GameTests.PreliminaryTestGames;
using Domain.Models.Lectures;
using Domain.Models.Teams;
using Domain.Models.Topics;
using System;
using System.Collections.Generic;

namespace Domain.Models.Games
{
    public enum GameTeamType
    {
        SingleTest,
        TeamTest
    }

    public class Game
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Topic Topic { get; set; }
        public Company Company { get; set; }
        public Image Preview { get; set; }
        public GameTeamType GameTeamType { get; set; }
        public ICollection<Team> Teams { get; set; }
        public ICollection<Lecture> Lectures { get; set; }
        public ICollection<PreliminaryTestGame> PreliminaryTestGames { get; set; }
        public ICollection<GeneralTestGame> GeneralTestGames { get; set; }
        public ICollection<FinalyTestGame> FinalyTestGames { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime? EndTime { get; set; }

        public bool IsActive()
        {
            return DateTime.Now >= StartTime && (EndTime == null || DateTime.Now < EndTime) ? true : false;
        }
    }
}