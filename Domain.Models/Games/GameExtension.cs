﻿using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Domain.Models.Games
{
    public static class GameExtension
    {
        public static IQueryable<Game> WithPreviewes(this IQueryable<Game> query)
        {
            return query
                .Include(g => g.Preview);
        }

        public static IQueryable<Game> WithPreliminaryTestGames(this IQueryable<Game> query)
        {
            return query
                .Include(g => g.PreliminaryTestGames)
                .ThenInclude(ptg => ptg.Test)
                .ThenInclude(t => t.Preview);
        }

        public static IQueryable<Game> WithGeneralTestGames (this IQueryable<Game> query)
        {
            return query
                .Include(g => g.GeneralTestGames)
                .ThenInclude(gtg => gtg.Test)
                .ThenInclude(t => t.Preview);
        }

        public static IQueryable<Game> WithFinalyTestGames(this IQueryable<Game> query)
        {
            return query
                .Include(g => g.FinalyTestGames)
                .ThenInclude(ftg => ftg.Test)
                .ThenInclude(t => t.Preview);
        }

        public static IQueryable<Game> WithLectures(this IQueryable<Game> query)
        {
            return query
                .Include(g => g.Lectures)
                .ThenInclude(l => l.Preview);
        }
    }
}