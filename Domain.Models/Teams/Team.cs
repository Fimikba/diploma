﻿using Domain.Models.Games;
using Domain.Models.Tests;
using Domain.Models.UserTeams;
using System.Collections.Generic;

namespace Domain.Models.Teams
{
    public class Team
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<UserTeam> UserTeams { get; set; }
        public Game Game { get; set; }
    }
}