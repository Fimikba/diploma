﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Domain.Models.UserLectures
{
    public class UserLectureEntityTypeConfiguration : IEntityTypeConfiguration<UserLecture>
    {
        public void Configure(EntityTypeBuilder<UserLecture> builder)
        {
            builder.HasKey(ul => new { ul.UserId, ul.LectureId });

            builder.HasOne(ul => ul.User)
                .WithMany(u => u.UserLectures)
                .HasForeignKey(ul => ul.UserId);

            builder.HasOne(ul => ul.Lecture)
                .WithMany(l => l.UserLectures)
                .HasForeignKey(ul => ul.LectureId);
        }
    }
}
