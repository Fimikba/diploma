﻿using Domain.Models.Lectures;
using Domain.Models.Users;

namespace Domain.Models.UserLectures
{
    public class UserLecture
    {
        public Lecture Lecture { get; set; }
        public int LectureId { get; set; }
        public User User { get; set; }
        public int UserId { get; set; }
        public bool LectureViewed { get; set; }
    }
}
