﻿using Domain.Models.Companies;
using Domain.Models.UserTests;
using Domain.Models.UserLectures;
using Domain.Models.UserTeams;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace Domain.Models.Users
{
    public class User : IdentityUser<int>
    {
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string Surname { get; set; }
        public Company Company { get; set; }
        public ICollection<UserTest> UserTests { get; set; }
        public ICollection<UserTeam> UserTeams { get; set; }
        public ICollection<UserLecture> UserLectures { get; set; }
    }
}