﻿using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Domain.Models.Users
{
    public static class UserExtension
    {
        public static IQueryable<User> UserWithCompany(this IQueryable<User> query)
        {
            return query
                .Include(u => u.Company);
        }
    }
}