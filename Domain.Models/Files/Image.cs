﻿namespace Domain.Models.Files
{
    public class Image : File
    {
        public int Height { get; set; }
        public int Width { get; set; }
    }
}
