﻿using Domain.Models.Tests;
using System.Collections.Generic;

namespace Domain.Models.Files
{
    public class File
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public ICollection<Test> Tests { get; set; }
    }
}
