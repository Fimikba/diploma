﻿using Domain.Models.Companies;

namespace Domain.Models.Lectures
{
    public class CustomLecture : Lecture
    {
        public Company AuthorCompany { get; set; }
        public bool IsPublic { get; set; }
    }
}