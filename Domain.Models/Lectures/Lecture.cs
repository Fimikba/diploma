﻿using System.Collections.Generic;
using Domain.Models.Files;
using Domain.Models.Games;
using Domain.Models.UserLectures;

namespace Domain.Models.Lectures
{
    public class Lecture
    {
        public int Id { get; set; }
        public string LectureName { get; set; }
        public File Material { get; set; }
        public ICollection<Game> Games { get; set; }
        public ICollection<UserLecture> UserLectures { get; set; }
        public Image Preview { get; set; }
    }
}
