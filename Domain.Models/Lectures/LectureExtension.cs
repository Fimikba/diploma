﻿using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Domain.Models.Lectures
{
    public static class LectureExtension
    {
        public static IQueryable<Lecture> WithMaterial(this IQueryable<Lecture> query)
        {
            return query
                .Include(l => l.Material);
        }
    }
}
