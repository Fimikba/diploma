﻿using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Domain.Models.Tests
{
    public static class TestExtension
    {
        public static IQueryable<Test> WithQuestions(this IQueryable<Test> query)
        {
            return query
                .Include(t => t.Questions)
                    .ThenInclude(q => q.Images)
                .Include(t => t.Questions)
                    .ThenInclude(q => q.Answers);
        }
    }
}