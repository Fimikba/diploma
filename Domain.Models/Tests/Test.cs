﻿using System;
using System.Collections.Generic;
using Domain.Models.Questions;
using Domain.Models.UserTests;
using Domain.Models.Files;
using Domain.Models.Games.GameTests.FinalyTestGames;
using Domain.Models.Games.GameTests.GeneralTestGames;
using Domain.Models.Games.GameTests.PreliminaryTestGames;
using Domain.Models.Answers;
using System.Linq;

namespace Domain.Models.Tests
{
    public enum TestType
    {
        NoLimitTimeTest,
        LimitTimeTest,
        QuestionTimeTest
    }

    public enum TopicStage 
    { 
        PreliminaryTest,
        Game,
        FinalTest
    }

    public class Test
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public TestType Type { get; set; }
        public ICollection<PreliminaryTestGame> PreliminaryTestGames { get; set; }
        public ICollection<GeneralTestGame> GeneralTestGames { get; set; }
        public ICollection<FinalyTestGame> FinalyTestGames { get; set; }
        public TopicStage TopicStage { get; set; }
        public TimeSpan? MaxTime { get; set; }
        public ICollection<Question> Questions { get; set; }
        public ICollection<UserTest> UserTests { get; set; }
        public File Preview { get; set; }

        public int MaxPoints()
        {
            int maxPoints = 0;

            foreach (var question in Questions)
            {
                foreach (var answer in question.Answers)
                {
                    if (answer.Points>0)
                    {
                        maxPoints += answer.Points;
                    }
                }
            }

            return maxPoints;
        }
    }
}