﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace Domain.Models.Tests
{
    public class AddTestModel
    {
        [Required]
        [Display(Name = "Название")]
        public string Name { get; set; } 

        [Display(Name = "Обложка")]
        public IFormFile Prewiew { get; set; } 

        [Required]
        [Display(Name="Тип теста")]
        public TestType Type { get; set; }

        [Required]
        [Display(Name = "Доступен всем пользователям?")]
        public bool IsPublic { get; set; } 

        [Required]
        [Display(Name = "Вид теста")]
        public TopicStage TopicStage { get; set; } 

        [Required]
        public int TopicId { get; set; }

        [Required]
        [Display(Name = "Сколько будет вопросов в тесте?")]
        public int NumberOfQuestionsInTest { get; set; }
    }
}