﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models.Roles
{
    public static class RoleNames
    {
        public const string Admin = "admin";
        public const string CompanyAdmin = "company_admin";
        public const string CompanyMember = "company_member";
    }
}
