﻿using Domain.Models.Tests;
using Domain.Models.Users;
using System;
using System.Collections.Generic;

namespace Domain.Models.UserTests
{
    public class UserTest
    {
        public User User { get; set; }
        public int UserId { get; set; }
        public Test Test { get; set; }
        public int TestId { get; set; }
        public ICollection<UserTestQuestion> UserTestQuestions { get; set; }
        public int? Points { get; set; }
        public DateTime? SpendTime { get; set; }
        public DateTime? PassTime { get; set; }
    }
}
