﻿using Domain.Models.Questions;
using System;

namespace Domain.Models.UserTests
{
    public class UserTestQuestion
    {
        public UserTest UserTest { get; set; }
        public int UserId { get; set; }
        public int TestId { get; set; }
        public Question Question { get; set; }
        public int QuestionId { get; set; }
        public int Points { get; set; }
        public DateTime? SpendTime { get; set; }

    }
}
