﻿using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Domain.Models.UserTests
{
    public static class UserTestExtension
    {
        public static IQueryable<UserTest> WithTest(this IQueryable<UserTest> query)
        {
            return query
                .Include(ut => ut.Test)
                .ThenInclude(t => t.Questions)
                .ThenInclude(q => q.Answers);
        }

        public static IQueryable<UserTest> WithQuestion (this IQueryable<UserTest> query)
        {
            return query
                .Include(ut => ut.UserTestQuestions);
        }

    }
}