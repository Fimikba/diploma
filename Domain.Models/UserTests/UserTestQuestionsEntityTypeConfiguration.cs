﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Domain.Models.UserTests
{
    public class UserTestQuestionsEntityTypeConfiguration : IEntityTypeConfiguration<UserTestQuestion>
    {
        public void Configure(EntityTypeBuilder<UserTestQuestion> builder)
        {
            builder.HasKey(utq => new { utq.TestId, utq.UserId, utq.QuestionId });

            builder.HasOne(utq => utq.UserTest)
                .WithMany(ut => ut.UserTestQuestions)
                .HasForeignKey(utq => new { utq.UserId, utq.TestId});

            builder.HasOne(utq => utq.Question)
                .WithMany(q => q.UserTestQuestions)
                .HasForeignKey(utq => utq.QuestionId);
        }
    }
}
