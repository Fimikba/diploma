﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Domain.Models.UserTests
{
    public class UserTestEntityTypeConfiguration : IEntityTypeConfiguration<UserTest>
    {
        public void Configure(EntityTypeBuilder<UserTest> builder)
        {
            builder.HasKey(ut => new { ut.TestId, ut.UserId });

            builder.HasOne(ut => ut.User)
                .WithMany(u => u.UserTests)
                .HasForeignKey(ut => ut.UserId);

            builder.HasOne(ut => ut.Test)
                .WithMany(g => g.UserTests)
                .HasForeignKey(ut => ut.TestId);
        }
    }
}
