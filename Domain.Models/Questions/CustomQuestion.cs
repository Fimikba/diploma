﻿using Domain.Models.Companies;

namespace Domain.Models.Questions
{
    public class CustomQuestion : Question
    {
        public Company AuthorCompany { get; set; }
        public bool IsPublic { get; set; }
    }
}