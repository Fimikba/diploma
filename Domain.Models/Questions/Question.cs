﻿using Domain.Models.Answers;
using Domain.Models.Files;
using Domain.Models.Tests;
using Domain.Models.UserTests;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Domain.Models.Questions
{
    public enum QuestionType
    {
        Single,
        Multy
    }

    public class Question
    {
        public int Id { get; set; }
        public ICollection<Test> Tests { get; set; }
        public QuestionType Type { get; set; }
        public DateTime? MaxTime { get; set; }
        public string Text { get; set; }
        public ICollection<Image> Images { get; set; }
        public ICollection<Answer> Answers { get; set; }
        public ICollection<UserTestQuestion> UserTestQuestions { get; set; }

        public int? Points()
        {
            return Answers?.Where(a => a.Points > 0).Select(a => a.Points).Sum();
        }
    }
}
