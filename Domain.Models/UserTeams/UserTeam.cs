﻿using Domain.Models.Teams;
using Domain.Models.Users;

namespace Domain.Models.UserTeams
{
    public enum TeamRole
    {
        Captain,
        Member
    }

    public class UserTeam
    {
        public int UserId { get; set; }
        public User User { get; set; }
        public int TeamId { get; set; }
        public Team Team { get; set; }
        public TeamRole Role { get; set; }
    }
}
