﻿using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Domain.Models.Companies
{
    public static class CompanyExtension
    {
        public static IQueryable<Company> WithCreatedAndUsedCustomeTopics(this IQueryable<Company> query)
        {
            return query
                .Include(c => c.CreatedTopics)
                .ThenInclude(t => t.Preview)
                .Include(c => c.CustomTopics)
                .ThenInclude(t => t.Preview);
        }
    }
}