﻿using System.Collections.Generic;
using Domain.Models.Questions;
using Domain.Models.Lectures;
using Domain.Models.Users;
using Domain.Models.Topics;
using Domain.Models.Tests;
using Domain.Models.Games;

namespace Domain.Models.Companies
{
    public class Company
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string AdminPublicRegistrationToken { get; set; }
        public string MemberPublicRegistrationToken { get; set; }
        public ICollection<User> Users { get; set; }
        public ICollection<Test> CreatedTests { get; set; }
        public ICollection<Question> CreatedQuestions { get; set; }
        public ICollection<CustomLecture> CreatedLectures { get; set; }
        public ICollection<CustomTopic> CreatedTopics { get; set; } 
        public ICollection<CustomTopic> CustomTopics { get; set; } 
        public ICollection<Game> Games { get; set; }
    }
}